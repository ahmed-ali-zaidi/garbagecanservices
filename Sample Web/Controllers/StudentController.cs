﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sample_Web.Models;
using Sample_Web.Helpers;
using DataServices;
using System.Configuration;

namespace Sample_Web.Controllers
{
    public class StudentController : Controller
    {
        DataClasses1DataContext _db;
        public StudentController()
        {
            _db = new DataClasses1DataContext();
        }
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        Student[] students = new Student[]
        {
            new Student() { StudentId=1, FirstName="Ali", LastName="Akbar"},
            new Student() { StudentId=2, FirstName="Ahmed", LastName="Ali" },
            new Student() { StudentId=3, FirstName="Abc", LastName="XYz" },
        };

        [HttpGet]
        public ActionResult getAllStudents()
        {
            return Json(students.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getSingleStudent(int id = 1)
        {
            return Json(students.FirstOrDefault(s => s.StudentId == id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddStudent(Student st)
        {
            var stList = students.ToList();
            stList.Add(st);
            students = stList.ToArray();
            return Json(stList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Init(InitRequest request)
        {

            GenericResponse<InitResponse> response = new GenericResponse<InitResponse>();
            DataClasses1DataContext db = new DataClasses1DataContext();

            InitResponse initResponse = new InitResponse();


            Customer customer = db.Customers.Where(x => x.DeviceID.Equals(request.DeviceID)).SingleOrDefault();
            Info info = db.Infos.FirstOrDefault();

            initResponse.customer = customer;
            initResponse.info = info;
            initResponse.items = db.Items.ToList();

            response.ResponseCode = "00";
            response.ResponseDesc = "OK";
            response.Data = initResponse;

            Logger.Info("App initiated for " + customer.Name);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCustomer(AddCustomerRequest request)
        {
            GenericResponse<Customer> response = new GenericResponse<Customer>();
            DataClasses1DataContext db = new DataClasses1DataContext();

            Customer customer = db.Customers.Where(x => x.DeviceID.Equals(request.DeviceId)).SingleOrDefault();

            if (customer != null)
            {
                response.ResponseCode = "01";
                response.ResponseDesc = "Customer Already Exist";
                response.Data = null;
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Customer newCustomer = new Customer()
            {
                DeviceID = request.DeviceId,
                Email = request.Email,
                Mobile = request.Mobile,
                Name = request.Name
            };

            db.Customers.InsertOnSubmit(newCustomer);
            db.SubmitChanges();

            response.ResponseCode = "00";
            response.ResponseDesc = "OK";
            response.Data = newCustomer;
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult AddSale(AddSaleRequest request)
        {

            GenericResponse<Sale> response = new GenericResponse<Sale>();
            DataClasses1DataContext db = new DataClasses1DataContext();

            Customer customer = db.Customers.Where(x => x.DeviceID.Equals(request.DeviceId)).SingleOrDefault();



            if (customer == null)
            {
                response.ResponseCode = "99";
                response.ResponseDesc = "Unknown error occured";
                response.Data = null;
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Sale sale = new Sale();
            sale.CustomerID = customer.id;

            db.Sales.InsertOnSubmit(sale);
            db.SubmitChanges();

            foreach (Sample_Web.Models.Item detail in request.Items)
            {
                SaleDetail temp = new SaleDetail()
                {
                    ItemName = detail.ItemName,
                    Quantity = detail.Quantity,
                    Size = detail.Size,
                    SaleID = sale.id
                };
                db.SaleDetails.InsertOnSubmit(temp);
                db.SubmitChanges();
            }

            response.ResponseCode = "00";
            response.ResponseDesc = "OK";
            response.Data = sale;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddSalePictures(int saleID)
        {
            GenericResponse<SalePicture> response = new GenericResponse<SalePicture>();
            DataClasses1DataContext db = new DataClasses1DataContext();
            try
            {
                var file = Request.Files[0];

                //String path = "~/sale_images/" + request.SaleId;
                String path = "~/sale_images/" + saleID;

                bool exists = System.IO.Directory.Exists(Server.MapPath(path));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath(path));

                string fileName = RandomString(30) + ".jpeg";
                string finalPath = Server.MapPath(path) + "/" + fileName;
                file.SaveAs(finalPath);

                SalePicture temp = new SalePicture()
                {
                    FilePath = finalPath,
                    SaleID = 1
                };

                db.SalePictures.InsertOnSubmit(temp);
                db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = temp;
            }
            catch (Exception e)
            {
                response.ResponseCode = e.Message;
                response.ResponseDesc = e.StackTrace.ToString();
                response.Data = null;
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        public JsonResult SendEmail(EmailRequest request)
        {
            GenericResponse<string> response = new GenericResponse<string>();
            try
            {
                string adminEmail = ConfigurationManager.AppSettings["adminEmail"].ToString();
                Customer customer = _db.Customers.FirstOrDefault(x => x.id == request.CustomerId);
                if (customer != null && !adminEmail.Trim().Equals(""))
                {
                    EmailSender.SendEmail(adminEmail, "Testing from app", request.EmailText);
                }
                else
                {
                    throw new Exception("Customer or Email address is not valid.");
                }
                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
            }
            catch (Exception ex)
            {
                response.ResponseCode = "01";
                response.ResponseDesc = ex.Message;
                Logger.Error(ex);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult AdminInit()
        {
            GenericResponse<InitResponse> response = new GenericResponse<InitResponse>();
            try
            {
                InitResponse entity = new InitResponse();
                entity.info = _db.Infos.FirstOrDefault();
                entity.items = _db.Items.ToList();
                entity.allCustomers = _db.Customers.ToList();
                response.Data = entity;
                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddConfiguration(Item item)
        {
            GenericResponse<bool> response = new GenericResponse<bool>();
            try
            {
                _db.Items.InsertOnSubmit(item);
                _db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateConfiguration(Item item)
        {
            GenericResponse<bool> response = new GenericResponse<bool>();
            try
            {
                var dbItem = _db.Items.FirstOrDefault(x => x.ItemId == item.ItemId);
                dbItem.ItemName = item.ItemName;
                dbItem.ItemPrice = item.ItemPrice;
                dbItem.ItemDescription = item.ItemDescription;
                dbItem.IsActive = item.IsActive;
                _db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateInfo(Info info)
        {
            GenericResponse<bool> response = new GenericResponse<bool>();
            try
            {
                var dbInfo = _db.Infos.LastOrDefault();
                dbInfo.AboutUs = info.AboutUs;
                dbInfo.Email = info.Email;
                dbInfo.Mobile = info.Mobile;
                _db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MarkCompleted(int id)
        {
            GenericResponse<bool> response = new GenericResponse<bool>();
            try
            {
                var order = _db.Sales.FirstOrDefault(x => x.id == id);
                order.IsCompleted = true;
                _db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAllOrders()
        {
            GenericResponse<List<OrderEntity>> response = new GenericResponse<List<OrderEntity>>();
            try
            {
                var sales = _db.Sales.Where(x => x.IsCompleted == false);
                foreach (var sale in sales)
                {
                    OrderEntity entity = new OrderEntity();
                    entity.sale = sale;
                    entity.saledetail = _db.SaleDetails.Where(x => x.SaleID == sale.id).ToList();
                    entity.salepicture = _db.SalePictures.Where(x => x.SaleID == sale.id).ToList();
                    response.Data.Add(entity);
                }
                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = null;
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult UpdateCustomer(int customerid, string isdirty, string isfavorite)
        {
            GenericResponse<bool> response = new GenericResponse<bool>();
            try
            {
                var customer = _db.Customers.FirstOrDefault(x => x.id == customerid);
                if (!String.IsNullOrEmpty(isdirty))
                {
                    customer.IsDirty = Convert.ToBoolean(isdirty);
                }
                if (!String.IsNullOrEmpty(isfavorite))
                {
                    customer.IsFavourite = Convert.ToBoolean(isfavorite);
                }
                _db.SubmitChanges();

                response.ResponseCode = "00";
                response.ResponseDesc = "OK";
                response.Data = true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                response.ResponseCode = "01";
                response.ResponseDesc = "Unable to connect this time";
                response.Data = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
    }


}