﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class AddSaleRequest
    {
        public string DeviceId { get; set; }
        public List<Item> Items { get; set; }


    }

    public class Item
    {
        public string ItemName { get; set; }
        public string Quantity { get; set; }
        public string Size { get; set; }
        public int SaleID { get; set; }
        public int id { get; set; }
    }
}