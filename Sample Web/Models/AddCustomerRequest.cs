﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class AddCustomerRequest
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string DeviceId { get; set; }
    }
}