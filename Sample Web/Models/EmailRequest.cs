﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class EmailRequest
    {
        public int CustomerId { get; set; }
        public string EmailText { get; set; }
    }
}