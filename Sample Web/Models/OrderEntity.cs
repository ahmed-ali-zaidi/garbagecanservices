﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class OrderEntity
    {
        public Sale sale { get; set; }
        public List<SaleDetail> saledetail { get; set; }
        public List<SalePicture> salepicture{ get; set; }


    }
}