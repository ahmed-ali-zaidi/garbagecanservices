﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class InitResponse
    {
        public Customer customer { get; set; }
        public Info info { get; set; }

        public List<Sample_Web.Item> items { get; set; }

        public List<Customer> allCustomers { get; set; }
    }
}