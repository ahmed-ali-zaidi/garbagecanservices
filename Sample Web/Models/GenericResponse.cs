﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample_Web.Models
{
    public class GenericResponse<T>
    {
        public string ResponseCode { get; set; }
        public string ResponseDesc { get; set; }
        public T Data { get; set; }

    }
}