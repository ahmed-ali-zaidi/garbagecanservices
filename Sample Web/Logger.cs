﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataServices
{
    public static class Logger
    {
        private static log4net.ILog Log { get; set; }

        static Logger()
        {
            Log = log4net.LogManager.GetLogger(typeof(Logger));
            //log4net.Config.XmlConfigurator.Configure();
        }

        public static void Error(object msg)
        {
            
            Log.Error(msg);
        }

        public static void Error(object msg, Exception ex)
        {
            Log.Error(msg, ex);
        }

        public static void Error(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        public static void Info(object msg)
        {
            Log.Info(msg);
        }
        public static void Debug(object msg) {

            Log.Debug(msg);
        }
    }
}